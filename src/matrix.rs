use generic_array::{typenum as typenum, GenericArray};

use generic_array::ArrayLength;
use typenum::{U2,U3,U4,Unsigned};

use crate::matrix_base::MatrixBase;
use num::Num;
use std::ops;

use crate::vector::*;

pub type SquareMatrix<T, N> = MatrixBase<T, N, N>;

impl <T, N> SquareMatrix<T,N>
where
    N: ArrayLength<T> + Unsigned + ops::Mul<N>,
    N: ArrayLength<T> + Unsigned,
    T: Num + Copy + Clone,
    <N as std::ops::Mul<N>>::Output: generic_array::ArrayLength<T> + Copy + Clone,
    <<N as std::ops::Mul<N>>::Output as generic_array::ArrayLength<T>>::ArrayType: Copy + Clone,
{
    pub fn identity() -> Self {
        let size = <N as Unsigned>::to_usize();

        let mut data = vec![T::zero(); size * size];
        for i in 0..size {
            data[i * (size + 1)] = T::one();
        }

        let data = GenericArray::clone_from_slice(data.as_slice());

        Self{
            data,
        }
    }
}

pub type Matrix2f = SquareMatrix<f32, U2>;
pub type Matrix3f = SquareMatrix<f32, U3>;
pub type Matrix4f = SquareMatrix<f32, U4>;

impl Matrix4f {
    pub fn translation (translate: &Vector3f) -> Self {
        let mut result = Self::identity();

        result.data[3] = translate.x();
        result.data[7] = translate.y();
        result.data[11] = translate.z();

        return result;
    }

    pub fn rotation (axis: &Vector3f, angle: f32) -> Self {
        let mut result = Self::identity();

        let cos_angle = angle.to_radians().cos();
        let one_cos_angle = 1.0 - cos_angle;
        let sin_angle = angle.to_radians().sin();

        let ux = axis.x();
        let ux_2 = ux * ux;
        let uy = axis.y();
        let uy_2 = uy * uy;
        let uz = axis.z();
        let uz_2 = uz * uz;

        result.data[0] = cos_angle + (ux_2 * one_cos_angle);
        result.data[1] = (ux * uy * one_cos_angle) - (uz * sin_angle);
        result.data[2] = (ux * uz * one_cos_angle) + (uy * sin_angle);

        result.data[4] = (ux * uy * one_cos_angle) + (uz * sin_angle);
        result.data[5] = cos_angle + (uy_2 * one_cos_angle);
        result.data[6] = (uy * uz * one_cos_angle) - (ux * sin_angle);

        result.data[8]  = (ux * uz * one_cos_angle) - (uy * sin_angle);
        result.data[9]  = (uy * uz * one_cos_angle) + (ux * sin_angle);
        result.data[10] = cos_angle + (uz_2 * one_cos_angle);

        return result;
    }

    pub fn scale (scale: &Vector3f) -> Self {
        let mut result = Self::identity();

        result.data[0]  = scale.x();
        result.data[5]  = scale.y();
        result.data[10] = scale.z();

        return result;
    }

    pub fn base (x_vec: &Vector3f, y_vec: &Vector3f, z_vec: &Vector3f) -> Self {
        let mut result = Self::identity();

        result.data[0] = x_vec.x();
        result.data[1] = x_vec.y();
        result.data[2] = x_vec.z();

        result.data[4] = y_vec.x();
        result.data[5] = y_vec.y();
        result.data[6] = y_vec.z();

        result.data[8]  = z_vec.x();
        result.data[9]  = z_vec.y();
        result.data[10] = z_vec.z();

        return result;
    }

    pub fn perspective (left: f32, right: f32, top: f32, bottom: f32, far: f32, near: f32) -> Self {
        let mut result = Self::identity();

        result.data[0] = (2.0 * near) / (right - left);
        result.data[1] = 0.0;
        result.data[2] = (right + left) / (right - left);
        result.data[3] = 0.0;

        result.data[4] = 0.0;
        result.data[5] = (2.0 * near) / (top - bottom);
        result.data[6] = (top + bottom) / (top - bottom);
        result.data[7] = 0.0;

        result.data[8]  = 0.0;
        result.data[9]  = 0.0;
        result.data[10] = -1.0 * (far + near) / (far - near);
        result.data[11] = -1.0 * (2.0 * far * near) / (far - near);

        result.data[12] = 0.0;
        result.data[13] = 0.0;
        result.data[14] = -1.0;
        result.data[15] = 0.0;

        return result;
    }

    pub fn orthographic (left: f32, right: f32, top: f32, bottom: f32, far: f32, near: f32) -> Self {
        let mut result = Self::identity();

        result.data[0] = 2.0 / (right - left);
        result.data[1] = 0.0;
        result.data[2] = 0.0;
        result.data[3] = 0.0;

        result.data[4] = 0.0;
        result.data[5] = 2.0 / (top - bottom);
        result.data[6] = 0.0;
        result.data[7] = 0.0;

        result.data[8]  = 0.0;
        result.data[9]  = 0.0;
        result.data[10] = -2.0 / (far - near);
        result.data[11] = 0.0;

        result.data[12] = -1.0 * (right + left) / (right - left);
        result.data[13] = -1.0 * (top + bottom) / (top - bottom);
        result.data[14] = -1.0 * (far + near) / (far - near);
        result.data[15] = 1.0;

        return result;
    }
}

pub type Matrix2x3f = MatrixBase<f32, U2, U3>;
pub type Matrix3x2f = MatrixBase<f32, U3, U2>;

pub type Matrix2x4f = MatrixBase<f32, U2, U4>;
pub type Matrix4x2f = MatrixBase<f32, U4, U2>;

pub type Matrix3x4f = MatrixBase<f32, U3, U4>;
pub type Matrix4x3f = MatrixBase<f32, U4, U3>;