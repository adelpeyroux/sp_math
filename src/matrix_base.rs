use std::ops;

use generic_array::typenum as typenum;

use generic_array::{ArrayLength, GenericArray};
use typenum::{Unsigned, Prod};
use num::{Num, FromPrimitive, Signed, abs};

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct MatrixBase <T, R, C>
where 
    C: ArrayLength<T> + Unsigned + ops::Mul<R>,
    R: ArrayLength<T> + Unsigned,
    T: Num + Copy + Clone,
    <C as std::ops::Mul<R>>::Output: generic_array::ArrayLength<T> + Copy + Clone,
    GenericArray<T, Prod<C,R>>: Copy + Clone,
    <<C as std::ops::Mul<R>>::Output as generic_array::ArrayLength<T>>::ArrayType: Copy + Clone,
{
    pub(crate) data: GenericArray<T, Prod<C,R>>,
}

impl <T, R, C> MatrixBase<T, R, C>
where
    C: ArrayLength<T> + Unsigned + ops::Mul<R>,
    R: ArrayLength<T> + Unsigned + ops::Mul<C>,
    T: Num + Copy + Clone,
    <C as std::ops::Mul<R>>::Output: generic_array::ArrayLength<T> + Copy + Clone,
    <<C as std::ops::Mul<R>>::Output as generic_array::ArrayLength<T>>::ArrayType: Copy + Clone,
    <R as std::ops::Mul<C>>::Output: generic_array::ArrayLength<T> + Copy + Clone,
    <<R as std::ops::Mul<C>>::Output as generic_array::ArrayLength<T>>::ArrayType: Copy + Clone,
{
    pub fn at (&self, i: usize, j: usize) -> T {
        let cols = <C as Unsigned>::to_usize();
        let index = i * cols + j;

        (self.data[index]).clone()
    }

    pub fn transpose (&self) -> MatrixBase<T, C, R> {
        let cols = <C as Unsigned>::to_usize();
        let rows = <R as Unsigned>::to_usize();

        let mut transposed_data = Vec::new();

        for j in 0..cols {
            for i in 0..rows {
                transposed_data.push(self.at(i,j));
            }
        }

        MatrixBase::<T, C, R> { data: GenericArray::<T, Prod<R, C>>::clone_from_slice(&transposed_data[..]) }
    }

    pub fn cols (&self) -> usize {
        <C as Unsigned>::to_usize()
    }

    pub fn rows (&self) -> usize {
        <R as Unsigned>::to_usize()
    }

    pub fn as_slice (&self) -> &[T] {
        self.data.as_slice()
    }
}

impl <T, R, C> MatrixBase<T, R, C> 
where
    C: ArrayLength<T> + Unsigned + ops::Mul<R>,
    R: ArrayLength<T> + Unsigned,
    T: Num + Signed + Copy + Clone,
    <C as std::ops::Mul<R>>::Output: generic_array::ArrayLength<T> + Copy + Clone,
    <<C as std::ops::Mul<R>>::Output as generic_array::ArrayLength<T>>::ArrayType: Copy + Clone,
{
    pub fn abs (self) -> Self {
        let mut new_data = self.data.clone();

        let cols = <C as Unsigned>::to_u32();
        let rows = <R as Unsigned>::to_u32();

        for i in 0..rows*cols {
            let index = i as usize;
            new_data[index] = abs(new_data[index]);
        } 

        Self { data: new_data }
    }
}

impl <T, R, C> ops::Mul<T> for MatrixBase<T, R, C> 
where
    C: ArrayLength<T> + Unsigned + ops::Mul<R>,
    R: ArrayLength<T> + Unsigned,
    T: Num + Copy + Clone,
    <C as std::ops::Mul<R>>::Output: generic_array::ArrayLength<T> + Copy + Clone,
    <<C as std::ops::Mul<R>>::Output as generic_array::ArrayLength<T>>::ArrayType: Copy + Clone,
{
    type Output = MatrixBase<T, R, C>;

    fn mul (self, value: T) -> Self {
        let mut new_data = self.data.clone();

        let cols = <C as Unsigned>::to_u32();
        let rows = <R as Unsigned>::to_u32();

        for i in 0..rows*cols {
            let index = i as usize;
            new_data[index] = new_data[index] * value;
        } 

        Self { data: new_data }
    }
}

impl <T, R, C> ops::Div<T> for MatrixBase<T, R, C> 
where
    C: ArrayLength<T> + Unsigned + ops::Mul<R>,
    R: ArrayLength<T> + Unsigned,
    T: Num + Copy + Clone,
    <C as std::ops::Mul<R>>::Output: generic_array::ArrayLength<T> + Copy + Clone,
    <<C as std::ops::Mul<R>>::Output as generic_array::ArrayLength<T>>::ArrayType: Copy + Clone,
{
    type Output = MatrixBase<T, R, C>;

    fn div (self, value: T) -> Self {
        let mut new_data = self.data.clone();

        let cols = <C as Unsigned>::to_u32();
        let rows = <R as Unsigned>::to_u32();

        for i in 0..rows*cols {
            let index = i as usize;
            new_data[index] = new_data[index] / value;
        } 

        Self { data: new_data }
    }
}

impl <T, R, C> ops::Add<T> for MatrixBase<T, R, C> 
where
    C: ArrayLength<T> + Unsigned + ops::Mul<R>,
    R: ArrayLength<T> + Unsigned,
    T: Num + Copy + Clone,
    <C as std::ops::Mul<R>>::Output: generic_array::ArrayLength<T> + Copy + Clone,
    <<C as std::ops::Mul<R>>::Output as generic_array::ArrayLength<T>>::ArrayType: Copy + Clone,
{
    type Output = MatrixBase<T, R, C>;

    fn add (self, value: T) -> Self {
        let mut new_data = self.data.clone();

        let cols = <C as Unsigned>::to_u32();
        let rows = <R as Unsigned>::to_u32();

        for i in 0..rows*cols {
            let index = i as usize;
            new_data[index] = new_data[index] + value;
        } 

        Self { data: new_data }
    }
}

impl <T, R, C> ops::Sub<T> for MatrixBase<T, R, C> 
where
    C: ArrayLength<T> + Unsigned + ops::Mul<R>,
    R: ArrayLength<T> + Unsigned,
    T: Num + Copy + Clone,
    <C as std::ops::Mul<R>>::Output: generic_array::ArrayLength<T> + Copy + Clone,
    <<C as std::ops::Mul<R>>::Output as generic_array::ArrayLength<T>>::ArrayType: Copy + Clone,
{
    type Output = MatrixBase<T, R, C>;

    fn sub (self, value: T) -> Self {
        let mut new_data = self.data.clone();

        let cols = <C as Unsigned>::to_u32();
        let rows = <R as Unsigned>::to_u32();

        for i in 0..rows*cols {
            let index = i as usize;
            new_data[index] = new_data[index] - value;
        } 

        Self { data: new_data }
    }
}

impl <T, R, C> ops::Add<MatrixBase <T, R, C>> for MatrixBase<T, R, C> 
where
    C: ArrayLength<T> + Unsigned + ops::Mul<R>,
    R: ArrayLength<T> + Unsigned,
    T: Num + Copy + Clone,
    <C as std::ops::Mul<R>>::Output: generic_array::ArrayLength<T> + Copy + Clone,
    <<C as std::ops::Mul<R>>::Output as generic_array::ArrayLength<T>>::ArrayType: Copy + Clone,
{
    type Output = MatrixBase<T, R, C>;

    fn add (self, _rhs: Self) -> Self {
        let mut new_data = self.data.clone();

        let cols = <C as Unsigned>::to_u32();
        let rows = <R as Unsigned>::to_u32();

        for i in 0..rows*cols {
            let index = i as usize;
            new_data[index] = new_data[index] + _rhs.data[index];
        } 

        Self { data: new_data }
    }
}

impl <T, R, C> ops::Sub<MatrixBase <T, R, C>> for MatrixBase<T, R, C> 
where
    C: ArrayLength<T> + Unsigned + ops::Mul<R>,
    R: ArrayLength<T> + Unsigned,
    T: Num + Copy + Clone,
    <C as std::ops::Mul<R>>::Output: generic_array::ArrayLength<T> + Copy + Clone,
    <<C as std::ops::Mul<R>>::Output as generic_array::ArrayLength<T>>::ArrayType: Copy + Clone,
{
    type Output = MatrixBase<T, R, C>;

    fn sub (self, _rhs: Self) -> Self {
        let mut new_data = self.data.clone();

        let cols = <C as Unsigned>::to_u32();
        let rows = <R as Unsigned>::to_u32();

        for i in 0..rows*cols {
            let index = i as usize;
            new_data[index] = new_data[index] - _rhs.data[index];
        } 

        Self { data: new_data }
    }
}

impl <T, S, R1, C2> ops::Mul<MatrixBase<T, S, C2>> for MatrixBase<T, R1, S>
where
    S: ArrayLength<T> + Unsigned + ops::Mul<R1>,
    C2: ArrayLength<T> + Unsigned + ops::Mul<S> + ops::Mul<R1>,
    R1: ArrayLength<T> + Unsigned,
    T: Num + FromPrimitive + Copy + Clone,
    <C2 as std::ops::Mul<S>>::Output: generic_array::ArrayLength<T> + Copy + Clone,
    <S as std::ops::Mul<R1>>::Output: generic_array::ArrayLength<T> + Copy + Clone,
    <C2 as std::ops::Mul<R1>>::Output: generic_array::ArrayLength<T> + Copy + Clone,
    <<C2 as std::ops::Mul<S>>::Output as generic_array::ArrayLength<T>>::ArrayType: Copy + Clone,
    <<C2 as std::ops::Mul<R1>>::Output as generic_array::ArrayLength<T>>::ArrayType: Copy + Clone,
    <<S as std::ops::Mul<R1>>::Output as generic_array::ArrayLength<T>>::ArrayType: Copy + Clone,
{
    type Output = MatrixBase<T, R1, C2>;

    fn mul (self, _rhs: MatrixBase <T, S, C2>) -> Self::Output {
        let res_rows = <R1 as Unsigned>::to_usize();
        let res_cols = <C2 as Unsigned>::to_usize();

        let common = <S as Unsigned>::to_usize();
        let mut final_data = Vec::new();

        for j in 0..res_rows {
            for i in 0..res_cols {
                let mut sum: T = T::zero();

                for s in 0..common {
                    let self_index = s + common * j;
                    let other_index = i + res_cols * s;

                    sum = sum + self.data[self_index] * _rhs.data[other_index];
                }
                final_data.push(sum);
            }
        }

        Self::Output { data: GenericArray::<T, Prod<C2, R1>>::clone_from_slice(&final_data[..]) }
    }
}

